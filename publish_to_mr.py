import os
import requests


if 'GITLAB_ACCESS_TOKEN' not in os.environ:
    raise Exception('GitLab Access Token Required')

if 'CI_MERGE_REQUEST_IID' not in os.environ:
    raise Exception('Merge Request not found for Pipeline')

if 'CI_PROJECT_ID' not in os.environ:
    raise Exception('Project not found for Pipeline')


GITLAB_ACCESS_TOKEN = os.environ.get('GITLAB_ACCESS_TOKEN') or ''
CI_MERGE_REQUEST_IID = os.environ.get('CI_MERGE_REQUEST_IID') or ''
CI_PROJECT_ID = os.environ.get('CI_PROJECT_ID') or ''


def get_file_content(path):
    if os.path.exists(path):
        fp = open(path, 'r')
        content = fp.read()
        fp.close()
        return content


def main():
    body = ''
    content = get_file_content('plan.publish_to_mr.txt')
    body = body + '\n'
    body = body + '\n## plan.publish_to_mr.txt'
    body = body + '\n'
    body = body + '\n' + '```' + content + '```'
    body = body + '\n'
    body = body + '---'
    url = 'https://gitlab.com/api/v4/projects/' + CI_PROJECT_ID + '/merge_requests/' + CI_MERGE_REQUEST_IID + '/notes'
    headers = {'private-token': GITLAB_ACCESS_TOKEN}
    params = {'id': CI_PROJECT_ID, 
              'merge_request_iid': CI_MERGE_REQUEST_IID,
              'body': body}
    r = requests.post(url=url,
                      headers=headers,
                      params=params,)
    print(url)
    print(params)
    print(headers)
    print(body)
    print(r.status_code)


main()
