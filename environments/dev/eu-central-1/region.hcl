locals {
  aws_region     = "eu-central-1"
  aws_region_azs = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
}
