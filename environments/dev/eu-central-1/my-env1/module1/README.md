# Module1

Please, do untar this folder under this relative path. Git detects there is a sub-module inside the inner folders of .terragrunt-cache and cannot add it normally.

```bash
$ pwd
terragrunt-cache-example/environments/dev/eu-central-1/my-env1/module1
$ tar -xvf .terragrunt-cache.tar.gz .terragrunt-cache
```
