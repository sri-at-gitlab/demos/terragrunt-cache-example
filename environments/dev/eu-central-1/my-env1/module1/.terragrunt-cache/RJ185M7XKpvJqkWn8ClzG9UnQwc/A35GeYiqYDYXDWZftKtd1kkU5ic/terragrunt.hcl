
locals {
  # Automatically load environment-level variables
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))

}

include {
  path = find_in_parent_folders()
}

terraform {
  # Use local module
  source = "git::https://github.com/terraform-aws-modules/terraform-aws-vpc.git//"
}

inputs = {
  name = "my-vpc"
  cidr = "10.0.0.0/16"

  aws_region      = local.region_vars.locals.aws_region
  region          = local.region_vars.locals.aws_region
  azs             = local.region_vars.locals.aws_region_azs
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = false
  enable_vpn_gateway = false

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
