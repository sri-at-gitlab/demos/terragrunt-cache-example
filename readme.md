# Terragrunt Cache Parser

1. This is an example of a Terragrunt project

The challenge faced with Terragrunt, as described by Marcos, is the creation of "magic" folders with completely random names.

For example, given this project path: `./environments/dev/eu-central-1/my-env1/module1`, you will find a `.terragrunt-cache` folder.

This `terragrunt-cache` folder itself contains a two-level nested folder set namely `./RJ185M7XKpvJqkWn8ClzG9UnQwc/A35GeYiqYDYXDWZftKtd1kkU5ic/`

This magic folder eventually contains a `terraform plan` which in this case is named `my_plan`

2. Introducing the `terragrunt_cache_parser.py`

```bash
./terragrunt_cache_parser.py --help
usage: terragrunt_cache_parser.py [-h] [--cache CACHE] [--plan PLAN]

Terragrunt Cache Parser parses the Terragrunt cache folder and returns the path to the Terraform plan.

optional arguments:
  -h, --help     show this help message and exit
  --cache CACHE  Path to the .terragrunt-cache folder
  --plan PLAN    Terraform plan name
```

`terragrunt_cache_parser.py` takes in two inputs:
- `cache` which is a path to the `.terragrunt-cache` folder, in this example it would be `environments/dev/eu-central-1/my-env1/module1/.terragrunt-cache`
- `plan` which is the name of the terraform plan created, in this example it would be `my_plan  `

3. `terragrunt_cache_parser.py` usage example:
```bash
./terragrunt_cache_parser.py --cache=environments/dev/eu-central-1/my-env1/module1/.terragrunt-cache --plan=my_plan

Terragrunt cache
 environments/dev/eu-central-1/my-env1/module1/.terragrunt-cache

Terragrunt cache :: First level
 environments/dev/eu-central-1/my-env1/module1/.terragrunt-cache/RJ185M7XKpvJqkWn8ClzG9UnQwc

Terragrunt cache :: Second level
 environments/dev/eu-central-1/my-env1/module1/.terragrunt-cache/RJ185M7XKpvJqkWn8ClzG9UnQwc/A35GeYiqYDYXDWZftKtd1kkU5ic

Terraform plan
 environments/dev/eu-central-1/my-env1/module1/.terragrunt-cache/RJ185M7XKpvJqkWn8ClzG9UnQwc/A35GeYiqYDYXDWZftKtd1kkU5ic/my_plan

Plan file path written to: plan_file_path.tmp.txt
```
