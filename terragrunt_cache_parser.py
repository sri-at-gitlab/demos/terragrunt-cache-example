#!/usr/bin/python

from os import listdir
from os.path import isdir, isfile, join

import argparse


def get_dirs(path):
    return  [f for f in listdir(path) if isdir(join(path, f))]


# parse args
arg_parser = argparse.ArgumentParser(description='Terragrunt Cache Parser parses the Terragrunt cache folder and returns the path to the Terraform plan.')
arg_parser.add_argument('--cache', 
                        help='Path to the .terragrunt-cache folder',
                        type=str,)
arg_parser.add_argument('--plan',
                        help='Terraform plan name',
                        type=str,)
args = arg_parser.parse_args()


# terragrunt cache
path_terragrunt_cache = args.cache or 'environments/dev/eu-central-1/my-env1/module1/.terragrunt-cache'

if not isdir(path_terragrunt_cache):
    print('Terragrunt cache path invalid:', path_terragrunt_cache)
    exit(1)


# terragrunt cache :: first level
root_child_dirs = get_dirs(path_terragrunt_cache)
if len(root_child_dirs) == 0:
    print('Found no children, is the Terragrunt cache path valid?', path_terragrunt_cache)
    exit(1)
path_first_level = join(path_terragrunt_cache, root_child_dirs[0])


# terragrunt cache :: second level
first_level_child_dirs = get_dirs(path_first_level)
path_second_level = join(path_first_level, first_level_child_dirs[0])


# terraform plan
plan_name = args.plan or 'my_plan'
path_plan = join(path_second_level, plan_name)
plan_exists = isfile(path_plan)


if not plan_exists:
    print('Error: Plan not found')
    print('\nTerragrunt cache\n', path_terragrunt_cache)
    print('\nTerragrunt cache :: First level\n', path_first_level, '\nIs dir?', isdir(path_first_level))
    print('\nTerragrunt cache :: Second level\n', path_second_level, '\nIs dir?', isdir(path_second_level))
    print('\nTerraform plan\n', path_plan, '\nIs file?', isfile(path_plan))
    exit(1)
else:
    f = open('plan_file_path.tmp.txt', 'w+')
    f.write(path_plan)
    f.close()
    print('\nTerragrunt cache\n', path_terragrunt_cache)
    print('\nTerragrunt cache :: First level\n', path_first_level)
    print('\nTerragrunt cache :: Second level\n', path_second_level)
    print('\nTerraform plan\n', path_plan)
    print('\nPlan file path written to:\n plan_file_path.tmp.txt')
